jQuery.exists = function (selector) { // функция проверки существования селектора
    return ($(selector).length > 0);
};

jQuery(function($){

    if($.exists(".bxslider")) {
        $('.bxslider').bxSlider({
            nextText: "",
            prevText: "",
            speed: 1000,
            auto: true,
            pause: 10000,
            autoHover: true,
            pager: false
        });
    }



    if($.exists(".arr-bot")) {
        $('.arr-bot').click(function(){
            if(document.getElementById($(this).attr('href').substr(1)) != null) {
                $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top }, 500);
            }
            return false;
        });
    }


    if($.exists("#book")) {
        $('#book').hover(
            function () {
               $(this).addClass('flip');
            }, function () {
                $(this).removeClass('flip');
            }
        );
    }



});